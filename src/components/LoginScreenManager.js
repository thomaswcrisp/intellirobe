import React from 'react'
import { StyleSheet, Platform, Image, Text, View } from 'react-native'
import { SwitchNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation'// import the different screens
import Loading from './screens/Loading'
import SignUp from './screens/SignUp'
import Login from './screens/Login'
import AppNavigator from './ScreenManager'
const AppContainer = AppNavigator

const LoginScreenManager = createSwitchNavigator(
    {
        Loading: {
            screen: Loading
        },
        SignUp: {
            screen: SignUp
        },
        Login: {
            screen: Login
        },
        AppContainer: {
            screen: AppContainer
        }
    },
    {
        initialRouteName: 'Login'
    }
)
export default LoginScreenManager