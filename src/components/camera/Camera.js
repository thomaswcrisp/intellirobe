import React, { PureComponent } from 'react'
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native'

import { RNCamera } from 'react-native-camera'


class Camera extends PureComponent {
    constructor(props) {
        super(props)

        state = {

        }
    }

    render() {
        return (
            
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
          <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
            <Text style={styles.headerStyle}>classify</Text>
          </TouchableOpacity>
        </View>
      </View>
        )
    }

    takePicture = async() => {
        if (this.camera) {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync(options);
        const predictions = await this.getPredictions(data.base64)
        

        // temporary
        // const predictions = {
        //   category: ['Jumpsuit', 'Hoodie', 'Joggers'],
        //   texture: 'Texture Result',
        //   fabric: 'Fabric Result',
        //   shape: 'Shape Result',
        //   parts: 'Parts Result',
        // };

        // const data = {
        //   uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkEBD4DwABWQEh/a6O2QAAAABJRU5ErkJggg==',
        //   base64: 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkEBD4DwABWQEh/a6O2QAAAABJRU5ErkJggg=='
        // };

        // console.log('Predictions:')
        // console.log(predictions)

        this.props.tagger(data.uri, predictions, data.base64)
        // also need to pass in predictions into below eventually:
        // this.props.tagger('test', data.uri, predictions)
        }
    };

    async getPredictions(imageData) {
        // 
        url = 'http://192.168.0.32/api'
        // url = 'http://192.168.0.14:8889/api'
        const response = await fetch(url, {
            method: 'POST', 
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                base64: imageData
            }),
        })

        const json = await response.json()

        
        return json 
        // .then((response) => response.json()).then((data) => {
        //     console.log(data)
        //     return data
        // }).catch((error) => {
        //     console.log("api call error")
        // });

        
        //  response.json())
        // .then((responseJson) => {
        //     return responseJson
        // })
    }

}

const styles = StyleSheet.create({
    preview: {
        flex: 2,
        justifyContent: 'flex-end',
        alignItems: 'center',
        bottom: 65,
    },
    capture: {
        flex: 0,
        height: 50,
        backgroundColor: 'white',
        borderRadius: 25,
        padding: 10,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent:'center',
        margin: 10,
        marginTop: 80
    },  
    container: {
        flex: 2,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    headerStyle: {
      textAlign: 'center', 
      flex: 1, 
      fontFamily: 'sans-serif-medium', 
      fontWeight:'200', 
      fontSize: 20, 
      color: '#AEA699'
  }
});

export default Camera