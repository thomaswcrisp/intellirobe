import React from "react";
import { View, Text, Button } from "react-native";
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createMaterialBottomTabNavigator } from "react-navigation";
import WardrobeHome from './screens/WardrobeHome'
import RecommenderScreen from './screens/Recommender'
import ClassifierScreen from './screens/Classifier'
import StylesScreen from './screens/Styles'
import SignOut from './screens/SignOut'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'



const AppNavigator = createBottomTabNavigator(
    {
        Recommender: {
            screen: RecommenderScreen,
            navigationOptions: {
                tabBarLabel: 'Recommender',
                tabBarIcon: () => (<View><Icon name='lightbulb-on-outline' size={35} color='#AEA699' /></View>)
                
            }
        },
        Wardrobe: {
            screen: WardrobeHome,
            navigationOptions: {
                tabBarLabel: 'Wardrobe Portal',
                tabBarIcon: () => (<View><Icon name='hanger' size={35} color='#AEA699' /></View>)
            }
        },
        Styles: {
            screen: StylesScreen,
            navigationOptions: {
                tabBarLabel: 'My Styles',
                tabBarIcon: () => (<View><Icon name='tshirt-crew-outline' size={35} color='#AEA699' /></View>)
            }
        },
        SignOut: {
            screen: SignOut,
            navigationOptions: {
                tabBarLabel: 'Logout',
                tabBarIcon: () => (<View><Icon name='reply' size={35} color='#AEA699' /></View>)
            }
        }
    },
    {
        initialRouteName: 'Wardrobe',
        activeTintColor: '#EDE9E6',
        barStyle: {
            labelStyle: {
                fontSize: 18,
                fontFamily: 'arial',
                textAlign: 'center',
                textAlignVertical: 'top'
            }
        }
    }
);

export default createAppContainer(AppNavigator)
