import React from "react";
import { Text, StyleSheet, View } from "react-native";

class MyAppHeaderText extends React.Component {

    constructor(props) {
        super(props)

    }

    render() {
      return (
          <View style={{textAlign: 'center', flex: 1}}>
          <Text style={styles.headerStyle}>{this.props.children}</Text>
          </View>
      );
    }
  }

  const styles = StyleSheet.create({
    headerStyle: {
        textAlign: 'center', 
        flex: 1, 
        // fontFamily: 'sans-serif-thin', 
        fontFamily: 'serif', 
        fontWeight:'200', 
        fontSize: 45, 
        color: '#AEA699', 
        marginTop: 5,
        borderRadius: 40, 
        borderBottomWidth: 3, 
        borderBottomColor: '#AEA699'
    }
  })

  export default MyAppHeaderText
  