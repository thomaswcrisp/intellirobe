import React, { Component } from "react";
import { View, Text, Button } from "react-native";
import { createStackNavigator, createAppContainer, ActivityIndicator } from "react-navigation";
import HeaderText from '../MyAppHeaderText';
import firebase from 'react-native-firebase';

class SignOut extends React.Component {



    render() {
      return (
        <View style={{ backgroundColor: '#AEA699', alignItems: 'center', justifyContent: 'center' }}>
          {/* <ActivityIndicator size="large"/> */}
        </View>
      );
    }

    componentDidMount() {
        firebase.auth().signOut().then(() => this.props.navigation.navigate('Login'))
        .catch(error => this.setState({ errorMessage: error.message
        }))
    }

}

const SignOutStack = createStackNavigator( {
    SignOut: {
        screen: SignOut
    }
})

export default SignOutStack