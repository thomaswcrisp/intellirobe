import React, { Component } from "react";
import { View, Text, Button, Platform, PermissionAndroid } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import WeatherCard from './WeatherCard';
import { FlatList } from "react-native-gesture-handler";
import Geolocation from 'react-native-geolocation-service';



class WeatherHandler extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            location: [],
            latitude: 0,
            longitude: 0,
            forecast: [],
            error: '',
        };
    }

    componentDidMount() {
        this.getLocation();
    }

    getLocation = () => {
        // Instead of navigator.geolocation, just use Geolocation.
        // if (hasLocationPermission) {
        const granted =  PermissionAndroid(PermissionAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

        if (granted) {
            Geolocation.getCurrentPosition(
                (position) => {
                    console.log(position);
                },
                (error) => {
                    // See error code charts below.
                    console.log(error.code, error.message);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
            );
        }
    };
    // }



    getWeather() {
        let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=02d14b710f7f4aa8d8d7c0d8ab45ff10'
        // call the API and set state of weather forecast

        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState((prevState, props) => ({
                    forecast: data
                }));
            })

    }

    render() {
        return (
            <View style={{ flex: 1, alignItem: 'center' }}>
                <FlatList
                    data={this.state.forecast.list}
                    style={{ marginTop: 20 }}
                    keyExtractor={item => item.dt_txt}
                    renderItem={
                        ({ item }) =>
                            <WeatherCard detail={item} location={this.state.forecast.city.name} />
                    } />
            </View>
        );
    }
}
// https://dev.to/andrewsmith1996/how-to-build-an-geolocation-weather-forecast-app-in-react-native-in-30-minutes-1kmo
export default WeatherHandler