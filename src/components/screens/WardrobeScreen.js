import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator, FlatList, List } from "react-native";
import { createStackNavigator } from "react-navigation";
import ClassifierScreen from './Classifier';
import HeaderText from '../MyAppHeaderText';
import { Tile, Card, Button, Icon, Image } from 'react-native-elements';
import { ScrollView } from "react-native-gesture-handler";
import Tags from 'react-native-tags';
import R from 'ramda';
import firebase from 'react-native-firebase';

const getItemKeys = R.compose(
    R.keys
)

const getItems = R.compose(
    R.values
)


class WardrobeScreen extends React.Component {

    constructor(props) {
        super(props)


        this.state = {
            wardrobeID: "",
            wardrobeName: "",
            wardrobeItems: [],
            itemKeys: [],
            clothingTypes: [],
            // wardrobeItemImg: ['./../../img/item1.jpg', './../../img/item2.jpg', './../../img/item3.jpg']
        }
    }


    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: <HeaderText >{navigation.getParam('wardrobeName')}</HeaderText>,
            headerTintColor: '#AEA699',
        };
    };

    componentDidMount() {
        const { navigation } = this.props;
        console.log(navigation.getParam('wardrobeID'))
        this.setState({ wardrobeID: navigation.getParam('wardrobeID', 'unknown'), wardrobeName: navigation.getParam('wardrobeName', 'unknown') })

        let pathString = 'wardrobes/' + firebase.auth().currentUser.uid + '/' + this.state.wardrobeID + 'clothes/';
        console.log(this.state.wardrobeID);

        firebase.database().ref('wardrobes/' + firebase.auth().currentUser.uid + '/' + navigation.getParam('wardrobeID') + '/clothes/').on('value', (data) => {
            data = data.toJSON();
            console.log('data is: ' + data);
            console.log('testing here in wardrobe screen');
            console.log(getItems(data));
            console.log(getItemKeys(data));
            this.setState({ wardrobeItems: getItems(data), itemKeys: getItemKeys(data) });
        })
    }

    render() {

        return (
            <View style={{ backgroundColor: '#ede9e6', width: '100%', height: '100%' }}>
                <View style={{ backgroundColor: '#ede9e6', borderBottomWidth: 1, borderBottomColor: '#AEA699' }}>
                    <TouchableOpacity style={{ flexDirection: 'row', textAlign: 'center', alignSelf: 'center' }} onPress={() => this.props.navigation.navigate('Classifier', { wardrobeID: this.state.wardrobeID })}>
                        <Text style={styles.topTitle}>add item to wardrobe </Text>
                        <Icon name='add' color='#AEA699' size={40} iconStyle={{ marginTop: 5 }} />
                    </TouchableOpacity>
                </View>

                <ScrollView style={{ flexDirection: 'column', backgroundColor: '#ede9e6' }}>
                    {
                        this.state.wardrobeItems.map((obj, index) => {
                            // imgPath = this.state.wardrobeItemImg[index]

                            console.log('rendering wardrobe item ' + index);
                            const removeClothingItem = itemID => () => {
                                const pathString = 'wardrobes/' + firebase.auth().currentUser.uid + '/' + this.state.wardrobeID + '/clothes/';
                                console.log(pathString)
                                firebase
                                    .database()
                                    .ref('wardrobes/' + firebase.auth().currentUser.uid + '/' + this.state.wardrobeID + '/clothes/')
                                    .child(itemID)
                                    .remove()
                                    .then((result) => { console.log(result) })
                                    .catch((error) => { console.log(error.message) });
                            }
                            return (
                                <View key={index} style={{ flexDirection: 'row', margin: 15, backgroundColor: 'white', height: 230, borderRadius: 15 }}>
                                    <Card
                                        title={obj.clothingClass}
                                        containerStyle={{ width: 150, height: 220, marginLeft: 15, marginTop: 5, borderRadius: 15, borderColor: '#AEA699' }}
                                        titleStyle={{ color: '#AEA699', fontSize: 20 }}
                                        image={{ uri: 'data:image/png;base64,' + obj.imageData }}
                                        // image={{uri: this.state.wardrobeItemImg}}
                                        imageStyle={{ width: 100, height: 100, alignSelf: 'center', borderRadius: 15 }}
                                    >
                                        <View style={{ flexDirection: 'row', alignSelf: 'center' }} onPress={() => console.log('the view was pressed')}>
                                            <Button
                                                icon={<Icon name='cancel' color='white' />}
                                                onPress={removeClothingItem(this.state.itemKeys[index])}
                                                buttonStyle={{ borderRadius: 15, marginLeft: 0, marginRight: 0, backgroundColor: '#AEA699' }}
                                                title='Remove' />
                                        </View>

                                    </Card>
                                    <View style={{ flex: 0, flexDirection: 'column', width: 200, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.midTitle}>attributes</Text>
                                        </View>
                                        <ScrollView style={{ marginTop: 35 }}>
                                            <Tags
                                                initialText=""
                                                textInputProps={{
                                                    placeholder: "tags of clothes"
                                                }}
                                                readonly={true}
                                                deleteTagOnPress={false}
                                                initialTags={R.values(obj.attributes)}
                                                onChangeTags={tags => console.log(tags)}
                                                containerStyle={{ flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', alignContent: 'flex-start' }}
                                                inputStyle={{ backgroundColor: "white" }}
                                                renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                                                    <TouchableOpacity key={`${tag}-${index}`} onPress={onPress} style={{ margin: 5, borderRadius: 50, backgroundColor: '#ebebeb', padding: 10 }}>
                                                        <Text>{tag}</Text>
                                                    </TouchableOpacity>
                                                )}
                                            />
                                        </ScrollView>
                                    </View>



                                </View>



                            )
                        })
                    }

                    <View style={{ height: 100 }}></View>

                </ScrollView>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    midTitle: {
        textAlign: 'center',
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 20,
        color: '#AEA699',
        marginTop: 5
    },
    topTitle: {
        textAlign: 'center',
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 20,
        color: '#AEA699',
        margin: 10
    }
})


const WardrobeScreenStack = createStackNavigator({
    Classifier: {
        screen: ClassifierScreen
    }
})



export default WardrobeScreen