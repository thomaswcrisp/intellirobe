import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text, Card, Divider, Button, Icon } from 'react-native-elements';
import StylesScreen from './Styles';
import R from 'ramda';
import { RadioButtons } from 'react-native-radio-buttons';


const extractWeatherDescription = R.pathOr('fjd', ['detail', 'weather', 0, 'description']);
const extractWeatherIcon = R.pathOr('fjd', ['detail', 'weather', 0, 'icon']);
const extractTemp = R.path(['main', 'temp']);
const extractWind = R.path(['wind', 'speed']);


export default class WeatherCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dressType: '',
            selectedOption: '',
            recommended: false,
            outfit: [{type: 'T-Shirt', imageData: 'lololol'}, {type: 'Jeans', imageData: 'trololol'}],
            // temperature: 0,
        }
    }

    buttonStyle = () => {
        let buttonColor = '';
        if (this.state.dressType == 'casual') {

        }
        return {
            borderRadius: 15,
            margin: 5,
            marginRight: 0,
            backgroundColor: '#AEA699'
        }
    }


    render() {
        const options = [
            "Casual",
            "Formal"
        ]

        function setSelectedOption(selectedOption) {
            this.setState({
                selectedOption
            });
            this.props.setStates(selectedOption, Math.round(extractTemp(this.props.detail)))
        }

        function renderOption(option, selected, onSelect, index) {
            const style = selected ? { fontWeight: 'bold', borderRadius: 15, margin: 5, marginTop: 15, marginRight: 0, backgroundColor: '#D7D8D8' } : { borderRadius: 15, margin: 5, marginTop: 15, marginRight: 0, backgroundColor: '#AEA699' };

            return (
                <Button
                    onPress={onSelect}
                    buttonStyle={style}
                    title={option}
                />
            );
        }

        function renderContainer(optionNodes) {
            return <View>{optionNodes}</View>;
        }

        let time;
        console.log('WEATHER CARD DATA RECEIVED');
        console.log(this.props.detail);
        //   console.log(this.props.detail.weather[0].main);
        console.log(extractTemp(this.props.detail));
        var date = new Date(this.props.detail.dt * 1000);

        var hours = date.getHours();

        var minutes = "0" + date.getMinutes();

        time = hours + ':' + minutes.substr(-2);

        return (
            //   <Text></Text>
            <View style={{ flex: 1, flexDirection: 'column', width: '100%', height: '100%' }}>
                {(this.state.selectedOption == '') ?<Text style={styles.topTitle}>please select which dress type you wish to advised for</Text> : <Text></Text>}
                <View style={{ flexDirection: 'row', flex: 0, marginTop: 15 }}>
                    <Card
                        containerStyle={styles.card}>
                        <Text style={styles.notes}>Weather in {this.props.location}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Image style={{ width: 50, height: 50 }} source={{ uri: "https://openweathermap.org/img/w/" + extractWeatherIcon(this.props) + ".png" }} />
                            <Text style={styles.time}>{extractWeatherDescription(this.props)}</Text>
                        </View>

                        <Divider style={{ backgroundColor: '#dfe6e9', marginVertical: 0 }} />

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.notes}>{windConversion(extractWind(this.props.detail))}</Text>
                            <Text style={styles.notes}>{Math.round(extractTemp(this.props.detail))}&#8451;</Text>
                        </View>
                    </Card>
                    <View style={{ width: '30%', flexDirection: 'column' }}>
                        <Text style={{ textAlign: 'center', fontFamily: 'serif', fontSize: 18, color: '#AEA699' }}>Dress Type</Text>
                        <View style={{ width: '90%', flexDirection: 'column' }}>
                            <RadioButtons
                                options={options}
                                onSelection={setSelectedOption.bind(this)}
                                selectedOption={this.state.selectedOption}
                                renderOption={renderOption}
                                renderContainer={renderContainer}
                            />
                            {/* <Button
                                // icon={<Icon name='cancel' color='white' />}
                                onPress={() => this.setState({ dressType: 'casual' })}
                                buttonStyle={{ borderRadius: 15, margin: 5, marginTop: 15, marginRight: 0, backgroundColor: '#AEA699' }}
                                title='Casual' />
                            <Button
                                // icon={<Icon name='cancel' color='white' />}
                                onPress={() => this.setState({ dressType: 'formal' })}
                                buttonStyle={{ borderRadius: 15, margin: 5, marginRight: 0, backgroundColor: '#AEA699' }}
                                title='Formal' /> */}
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

windConversion = (speed) => {
    speed = Math.round(speed);
    if (between(speed, 0, 5)) {
        return 'Calm'
    }
    else if (between(speed, 5, 19)) {
        return 'Gentle Breeze'
    }
    else if (between(speed, 20, 38)) {
        return 'Moderate Breeze'
    }
    else if (between(speed, 39, 49)) {
        return 'Strong Breeze'
    }
    else if (between(speed, 50, 74)) {
        return 'Gale'
    }
    else {
        return 'Storm winds'
    }
}

between = (x, min, max) => {
    return x >= min && x <= max;
}


const styles = {
    card: {
        flex: 0,
        backgroundColor: '#AEA699',
        borderWidth: 0,
        borderRadius: 20,
        width: '55%',
        marginLeft: 15,

    },
    time: {
        fontSize: 24,
        color: '#fff',
    },
    notes: {
        fontSize: 18,
        color: '#fff',
        textTransform: 'capitalize'
    },
    topTitle: {
        textAlign: 'center',
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 20,
        color: '#AEA699',
        margin: 10
    },
    topTitleTwo: {
        textAlign: 'center',
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 35,
        color: '#AEA699',
        margin: 10
    }
}