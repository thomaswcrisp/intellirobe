import React, { PureComponent, useMemo, useState, useCallback, useReducer, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { Dropdown } from 'react-native-material-dropdown';
import R from 'ramda';
import Tags from 'react-native-tags';

import MyAppHeaderText from '../MyAppHeaderText';
import Camera from '../camera/Camera';
import HeaderText from '../MyAppHeaderText';
import { useClassifier } from '../../modules/classifier';

const styles = StyleSheet.create({
    container: {
        height: '50%',
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    headerStyle: {
        textAlign: 'center',
        flex: 1,
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 45,
        color: '#AEA699',
        marginTop: 5,
        marginRight: 58,
    }
});

const Classifier = ({ navigation }) => {
    const wardrobeID = navigation.getParam('wardrobeID', 'unknown');
    const {
        state,
        processPrediction,
        selectCategory,
        removeAttribute,
        addClassifiedItem,
    } = useClassifier(wardrobeID);

    const addItemAndNavigate = useCallback(() =>
        addClassifiedItem().then(() => navigation.navigate('WardrobeScreen')), [addClassifiedItem]);

    return (
        <View style={{ width: '100%', height: '100%' }}>
            <View style={styles.container}>
                <Camera tagger={processPrediction} />
            </View>
            {state.isClassified
                ?
                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', flex: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ start: 0, width: 100, height: 100, backgroundColor: '#ebebeb', borderRadius: 20, margin: 20, }} source={{ uri: state.imageIcon }} />
                            <View style={{ flexDirection: 'column', flex: 0, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ flex: 1, fontSize: 20, fontFamily: 'sans-serif', fontWeight: 'bold' }}>Predicted as...</Text>
                                <Dropdown
                                    fontSize={35}
                                    animationDuration={1}
                                    value={state.selectedCategory}
                                    containerStyle={{ flex: 1, width: 200 }}
                                    itemTextStyle={{ fontSize: 35, fontFamily: 'serif', color: '#AEA699' }}
                                    style={{ fontFamily: 'serif' }}
                                    data={R.map(R.objOf('value'), state.predictedCategories)}
                                    onChangeText={selectCategory}
                                />
                            </View>
                        </View>
                        <View>
                            <View style={{ flexDirection: 'row', flex: 0, justifyContent: 'center' }}>
                                <Text style={{ flex: 0, fontSize: 30, fontFamily: 'sans-serif-thin', fontWeight: 'bold', justifyContent: 'center' }}>Predicted attributes:</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ flex: 0, fontSize: 14, justifyContent: 'center', fontFamily: 'sans-serif-thin' }}>tap irrelevant tags to delete them!</Text>
                            </View>
                            <View style={{ margin: 15 }}>
                                <Tags
                                    initialText=""
                                    textInputProps={{
                                        placeholder: "tags of clothes"
                                    }}
                                    readonly={true}
                                    deleteTagOnPress={true}
                                    tagContainerStyle={{ backgroundColor: '#AEA699' }}
                                    tagTextStyle={{ fontFamily: 'sans-serif', color: 'white' }}
                                    initialTags={R.pluck('prediction', state.attributes)}
                                    onTagPress={removeAttribute}
                                    containerStyle={{ justifyContent: "center" }}
                                    inputStyle={{ backgroundColor: "white", fontFamily: 'sans-serif-thin' }}
                                    renderTag={({ tag, index, onPress }) => (
                                        <TouchableOpacity key={`${tag}-${index}`} onPress={onPress} style={{ margin: 5, borderRadius: 50, backgroundColor: '#ebebeb', padding: 10 }}>
                                            <Text>{tag}</Text>
                                        </TouchableOpacity>
                                    )}
                                />
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
                                <TouchableOpacity onPress={addItemAndNavigate} style={{ height: 60, margin: 5, borderRadius: 50, backgroundColor: '#ebebeb', padding: 10 }}>
                                    <Text style={{ fontSize: 22, fontFamily: 'sans-serif-thin' }}>add to my wardrobe</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                :
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <MyAppHeaderText>Get your clothing in shot {'\n'} hit classify {'\n'} and let Intellirobe work its magic!</MyAppHeaderText>
                </View>
            }
        </View>
    );
};

Classifier.navigationOptions = {
    headerTitle: <HeaderText style={styles.headerStyle} >Classifier</HeaderText>,
    headerTintColor: '#AEA699',
};

export default Classifier;
