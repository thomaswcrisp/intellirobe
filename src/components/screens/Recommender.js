import React, { Component } from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import WeatherHandler from "./WeatherHandler";
import HeaderText from "../MyAppHeaderText";
import { Card, Divider, Button, Icon } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import Tags from "react-native-tags";
import R from "ramda";

import { useRecommender } from "../../modules/recommender";

const getItems = R.values;

const RecommenderScreen = props => {
  const {
    state,
    resetPage,
    setRecommendationState,
    selectWardrobe,
    saveOutfit,
    renderRecommender,
    resetAndRenderRecommender
  } = useRecommender(props);

  return (
    <ScrollView
      contentContainerStyle={{
        alignItems: "center",
        justifyContent: "flex-start"
      }}
      style={{ flex: 1 }}
    >
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "flex-start",
          marginBottom: 30
        }}
      >
        <WeatherHandler setStates={setRecommendationState}></WeatherHandler>
      </View>
      {/* data stored within the dropdown  */}
      {state.wardrobes.length > 0 ? (
        <Dropdown
          fontSize={35}
          animationDuration={1}
          value={state.selectedWardrobe.wardrobeName}
          containerStyle={{ flex: 1, width: 200 }}
          itemTextStyle={{
            fontSize: 35,
            fontFamily: "serif",
            color: "#AEA699"
          }}
          style={{ fontFamily: "serif" }}
          data={R.map(
            R.compose(
              R.objOf("value"),
              R.prop("wardrobeName")
            ),
            state.wardrobes
          )}
          onChangeText={selectWardrobe}
        />
      ) : (
        <Text style={styles.topTitle}>
          You need a wardrobe in order to be recommended!
        </Text>
      )}
      <Text style={styles.topTitleTwo}>Your Outfit</Text>
      <View style={{ flexDirection: "column", flex: 0 }}>
        <Divider style={{ backgroundColor: "#AEA699", marginVertical: 5 }} />
        {!state.initiateRecommend && (
          <Text style={styles.topTitle}>
            tap the recommend button to get todays outfit!
          </Text>
        )}
      </View>
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "flex-start"
        }}
      >
        {state.initiateRecommend && state.processing == false && (
          <ScrollView>
            {getItems(state.outfit).length > 0 ? (
              getItems(state.outfit).map((obj, index) => {
                // return null;
                return (
                  <View
                    key={index}
                    style={{
                      flexDirection: "row",
                      margin: 15,
                      backgroundColor: "white",
                      height: 230,
                      borderRadius: 15
                    }}
                  >
                    <Card
                      title={obj.clothingClass}
                      containerStyle={{
                        width: 150,
                        height: 220,
                        marginLeft: 15,
                        marginTop: 5,
                        borderRadius: 15,
                        borderColor: "#AEA699"
                      }}
                      titleStyle={{ color: "#AEA699", fontSize: 20 }}
                      image={{
                        uri: "data:image/png;base64," + obj.imageData
                      }}
                      // image={{uri: state.wardrobeItemImg}}
                      imageStyle={{
                        width: 100,
                        height: 100,
                        alignSelf: "center",
                        borderRadius: 15
                      }}
                    >
                      <View
                        style={{ flexDirection: "row", alignSelf: "center" }}
                      >
                        {/* Potentially add the change button below, depending on whether change individual item feature implemented */}
                        {/* <Button
                            icon={<Icon name='cancel' color='white' />}
                            onPress={removeClothingItem(state.itemKeys[index])}
                            buttonStyle={{ borderRadius: 15, marginLeft: 0, marginRight: 0, backgroundColor: '#AEA699' }}
                            title='Change' /> */}
                      </View>
                    </Card>
                    <View
                      style={{
                        flex: 0,
                        flexDirection: "column",
                        width: 200,
                        alignContent: "center",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text style={styles.midTitle}>attributes</Text>
                      </View>
                      <ScrollView style={{ marginTop: 35 }}>
                        <Tags
                          initialText=""
                          textInputProps={{
                            placeholder: "tags of clothes"
                          }}
                          readonly={true}
                          deleteTagOnPress={false}
                          initialTags={R.values(obj.attributes)}
                          containerStyle={{
                            flex: 1,
                            alignSelf: "center",
                            alignItems: "center",
                            justifyContent: "center",
                            alignContent: "flex-start"
                          }}
                          inputStyle={{ backgroundColor: "white" }}
                          renderTag={({
                            tag,
                            index,
                            onPress,
                            deleteTagOnPress,
                            readonly
                          }) => (
                            <TouchableOpacity
                              key={`${tag}-${index}`}
                              onPress={onPress}
                              style={{
                                margin: 5,
                                borderRadius: 50,
                                backgroundColor: "#ebebeb",
                                padding: 10
                              }}
                            >
                              <Text>{tag}</Text>
                            </TouchableOpacity>
                          )}
                        />
                      </ScrollView>
                    </View>
                  </View>
                );
              })
            ) : (
              <View>
                <Text style={styles.topTitle}>
                  SORRY YOUR WARDROBE IS INCOMPATIBLE WITH RECOMMENDATION!
                </Text>
                <Text style={styles.topTitle}>
                  either through incompatible clothes or too few in your
                  wardrobe!
                </Text>
                <Text style={styles.topTitle}>
                  please go to the wardrobe portal and add more to your
                  wardrobe: {state.selectedWardrobe.wardrobeName}
                </Text>
              </View>
            )}
          </ScrollView>
        )}
        {!state.initiateRecommend ? (
          <Button
            // icon={<Icon name='cancel' color='white' />}
            onPress={renderRecommender}
            buttonStyle={{
              borderRadius: 15,
              margin: 5,
              marginTop: 15,
              marginRight: 0,
              backgroundColor: "#AEA699"
            }}
            titleStyle={{ fontSize: 25, fontFamily: "sans" }}
            title="recommend"
          />
        ) : getItems(state.outfit).length > 0 ? (
          <View style={{ flexDirection: "row" }}>
            <Button
              // icon={<Icon name='cancel' color='white' />}
              onPress={saveOutfit}
              buttonStyle={{
                borderRadius: 15,
                margin: 5,
                marginTop: 15,
                marginRight: 0,
                backgroundColor: "#AEA699"
              }}
              titleStyle={{ fontSize: 25, fontFamily: "sans" }}
              title="save outfit"
            />
            <Button
              // icon={<Icon name='cancel' color='white' />}
              onPress={resetPage}
              buttonStyle={{
                borderRadius: 15,
                margin: 5,
                marginTop: 15,
                marginRight: 0,
                backgroundColor: "#AEA699"
              }}
              titleStyle={{ fontSize: 25, fontFamily: "sans" }}
              title="discard recommendation"
            />
          </View>
        ) : (
          <Button
            // icon={<Icon name='cancel' color='white' />}
            onPress={resetAndRenderRecommender}
            buttonStyle={{
              borderRadius: 15,
              margin: 5,
              marginTop: 15,
              marginRight: 0,
              backgroundColor: "#AEA699"
            }}
            titleStyle={{ fontSize: 25, fontFamily: "sans" }}
            title="recommend"
          />
        )}
      </View>
    </ScrollView>
  );
};

RecommenderScreen.navigationOptions = {
  headerTitle: <HeaderText>Fashion Adviser</HeaderText>
};

const RecommenderStack = createStackNavigator({
  RecommenderScreen: {
    screen: RecommenderScreen
  }
});

const styles = {
  topTitle: {
    textAlign: "center",
    fontFamily: "sans-serif-thin",
    fontWeight: "200",
    fontSize: 20,
    color: "#AEA699",
    margin: 10
  },
  topTitleTwo: {
    textAlign: "center",
    fontFamily: "sans-serif-thin",
    fontWeight: "200",
    fontSize: 35,
    color: "#AEA699",
    margin: 10
  }
};

export default RecommenderStack;
