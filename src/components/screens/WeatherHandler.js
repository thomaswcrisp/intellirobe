//This is an example code to get Geolocation// 

import React from 'react';
//import react in our code. 

import { View, Text, StyleSheet, Image, PermissionsAndroid, Platform, FlatList } from 'react-native';
//import all the components we are going to use.
import Geolocation from 'react-native-geolocation-service';
import WeatherCard from './WeatherCard';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentLongitude: 'unknown',//Initial Longitude
            currentLatitude: 'unknown',//Initial Latitude
            forecast: {},
            error: '',
            location: '',
        }
    }

    componentDidMount = () => {
        var that = this;
        //Checking for the permission just after component loaded
        if (Platform.OS === 'ios') {
            this.callLocation(that);
        } else {
            async function requestLocationPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                            'title': 'Location Access Required',
                            'message': 'This App needs to Access your location'
                        }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //To Check, If Permission is granted
                        that.callLocation(that);
                    } else {
                        alert("Permission Denied");
                    }
                } catch (err) {
                    alert("err", err);
                    console.warn(err)
                }
            }
            requestLocationPermission();
        }
    }



    callLocation(that) {
        //alert("callLocation Called");
        Geolocation.getCurrentPosition(
            (position) => {
                that.setState( (prevState) => ({
                    currentLatitude: position.coords.latitude, currentLongitude: position.coords.longitude }), () => { this.getWeather(); }
                );
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    getWeather() {
        let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.currentLatitude + '&lon=' + this.state.currentLongitude + '&units=metric&appid=02d14b710f7f4aa8d8d7c0d8ab45ff10'
        // call the API and set state of weather forecast
        fetch(url)
            .then(response => response.json())
            .then(data => {
                console.log('WEATHER HANDLER DATA')
                console.log(data);
                console.log('WEATHER HANDLER DATA FIRST ELEMENT')
                console.log(data.list[0]);
                let data2 = data.list[0]
                
                this.setState((prevState, props) => ({
                    forecast: data2,
                    location: data.city.name,
                }));
            })
    }

    componentWillUnmount = () => {
        navigator.geolocation.clearWatch(this.watchID);
    }


    render() {
        console.log(this.state.forecast)
        return (
            // <Text></Text>
            <WeatherCard setStates={this.props.setStates} detail={this.state.forecast} location={this.state.location}></WeatherCard>
            // <FlatList data={this.state.forecast.list} style={{marginTop:20}} keyExtractor={item => item.dt_txt} renderItem={({item}) => <WeatherCard detail={item} location={this.state.forecast.city.name} />} />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50,
        padding: 16,
        backgroundColor: 'white'
    },
    boldText: {
        fontSize: 30,
        color: 'red',
    }
})