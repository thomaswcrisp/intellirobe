import React from 'react';
import { StyleSheet, Text, TextInput, View, Button, TouchableOpacity, Keyboard, ToastAndroid } from 'react-native';
import HeaderText from '../MyAppHeaderText';
import firebase from 'react-native-firebase';


export default class NewWardrobe extends React.Component {
    
    static navigationOptions = {
        headerTitle: <HeaderText>Add Wardrobe</HeaderText>,
        headerTintColor: '#AEA699',
    }
    
    constructor(props) {
        super(props);
        this.state = { wardrobeName: '', description: '', errorMessage: null, displayIntro: true, marginText: '30%', currentUser: firebase.auth() };
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ displayIntro: false, marginText: '10%' })
    }
    
      _keyboardDidHide () {
          this.setState({ displayIntro: true, marginText: '30%'})
    }

    addWardrobe = () => {
        // TODO: add firebase functionality
        if(this.state.wardrobeName.length > 0 && this.state.description.length > 0) {
            if(this.state.description.length > 500) {
                alert('Your description is too long, please shorten it');
            } else {
                firebase.database().ref('wardrobes/' + firebase.auth().currentUser.uid + '/').push({
                    'wardrobeName': this.state.wardrobeName,
                    'description': this.state.description
                }).then(() => this.props.navigation.navigate('Wardrobe'))
            }
        } else {
            alert('please fill in the wardrobe name and description boxes')
        }
        // firebase.database().ref('wardrobes').push('test').then(() => { console.log('inserted succesfully')}).catch((error) => { console.log(error.message)})
    }

    render() {
        return (
            <View style={styles.container}>
            {/* <Text style={styles.mainTitleStyle}>Create Wardrobe</Text> */}
            {this.state.displayIntro && <Text style={{color: '#AEA699', fontFamily: 'sans-serif-thin', fontSize: 25, padding: 15, marginTop:15, textAlign: 'center' }}>creating a new wardrobe is easy, give your wardrobe a name and description and hit create.</Text>}
                <View style={{flex: 2, justifyContent: 'center', alignItems: 'center', marginBottom: this.state.marginText }}>
                    {/* if error message is non-null */}
                    {this.state.errorMessage &&
                        <Text style={{ color: 'red' }}>
                            {this.state.errorMessage}
                        </Text>}
                    <TextInput
                        placeholder="Wardrobe Name"
                        autoCapitalize="none"
                        style={styles.textInput}
                        // set the state of the TextInput to equal the text input onChangeText
                        onChangeText={wardrobeName => this.setState({ wardrobeName:  wardrobeName })}
                        onSubmitEditing={Keyboard.dismiss}
                        value={this.state.wardrobeName}
                    />
                    <TextInput
                        placeholder="Description"
                        autoCapitalize="none"
                        style={styles.descriptionInput}
                        onSubmitEditing={Keyboard.dismiss}
                        onChangeText={description => this.setState({ description })}
                        value={this.state.description}
                    />
                    <TouchableOpacity onPress={this.addWardrobe} style={styles.buttonStyle}>
                        <Text style={{color: 'white', fontFamily: 'sans-serif-thin', fontSize: 18, padding: 5}}>Create Wardrobe</Text>
                    </TouchableOpacity>
                </View>
             </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        height: 60,
        width: 250,
        borderColor: '#AEA699',
        borderRadius: 15,
        textAlign: 'center',
        fontSize: 20,
        borderWidth: 1,
        marginTop: 8,
        // backgroundColor: '#EFF0F1'
    },
    descriptionInput: {
        height: 120,
        width: 250,
        borderColor: '#AEA699',
        borderRadius: 15,
        textAlign: 'center',
        borderWidth: 1,
        marginTop: 8,
    },
    headerStyle: {
        textAlign: 'center', 
        fontFamily: 'sans-serif-thin', 
        fontWeight:'200', 
        fontSize: 45, 
        color: '#AEA699', 
        marginTop: 50,
        // borderRadius: 40, 
        // borderBottomWidth: 2, 
        // borderBottomColor: '#AEA699'
    },
    mainTitleStyle: {
        textAlign: 'center', 
        fontFamily: 'serif', 
        fontWeight:'200', 
        fontSize: 65, 
        width: '100%',
        // backgroundColor: '#EFF0F1',
        alignItems: 'center',
        // marginBottom: '60%',
        marginTop: 100,
        justifyContent: 'center',
        color: '#AEA699', 
        marginTop: 5,
        borderRadius: 100, 
        borderBottomWidth: 26, 
        borderBottomColor: '#AEA699'
    },
    buttonStyle: {
        borderRadius: 15, 
        marginTop: 15,
        margin: 5, 
        padding: 3,
        backgroundColor: '#AEA699'
    }
})