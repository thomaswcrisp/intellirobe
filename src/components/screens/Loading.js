import React from 'react';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';
import HeaderText from '../MyAppHeaderText';
import firebase from 'react-native-firebase'

export default class Loading extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <HeaderText>Loading</HeaderText>
                <ActivityIndicator size="large" />
            </View>
        )
    }

    // called after DOM has been rendered - causes rerendering
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.props.navigation.navigate(user ? 'AppContainer' : 'SignUp')
        })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})