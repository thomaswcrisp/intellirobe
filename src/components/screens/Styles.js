import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator, FlatList, List } from "react-native";
import { createStackNavigator } from "react-navigation";
import ClassifierScreen from './Classifier';
import HeaderText from '../MyAppHeaderText';
import { Tile, Card, Button, Icon, Image } from 'react-native-elements';
import { ScrollView } from "react-native-gesture-handler";
import Tags from 'react-native-tags';
import R from 'ramda';
import firebase from 'react-native-firebase';

const getItemKeys = R.compose(
  R.keys
);

const getItems = R.compose(
  R.values
);

const getSavedOutfits = R.compose(
  R.pluck('outfit'),
  R.values()
);

const getSavedOutfitDates = R.compose(
  R.pluck('date'),
  R.values()
);


class StylesScreen extends React.Component {

  constructor(props) {
    super(props)


    this.state = {
      savedOutfits: [],
      savedOutfitDates: [],
    }
  }


  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <HeaderText >{navigation.getParam('wardrobeName')}</HeaderText>,
      headerTintColor: '#AEA699',
    };
  };

  componentDidMount() {
    const { navigation } = this.props;

    firebase.database().ref('wardrobes/' + firebase.auth().currentUser.uid + '/savedOutfits/').on('value', (data) => {
      data = data.toJSON();
      console.log('data is: ', data);
      savedOutfits = getSavedOutfits(data);
      savedOutfitDates = getSavedOutfitDates(data);
      this.setState({ savedOutfits: savedOutfits, savedOutfitDates: savedOutfitDates });
    })
  }

  render() {
    console.log('saved outfits: ', this.state.savedOutfits);
    return (
      <View style={{ backgroundColor: '#ede9e6', width: '100%', height: '100%' }}>
        <View style={{ backgroundColor: '#ede9e6', borderBottomWidth: 1, borderBottomColor: '#AEA699' }}>
          <TouchableOpacity style={{ flexDirection: 'row', textAlign: 'center', alignSelf: 'center' }} onPress={() => this.props.navigation.navigate('Classifier', { wardrobeID: this.state.wardrobeID })}>
            <Text style={styles.topTitle}>add item to wardrobe </Text>
            <Icon name='add' color='#AEA699' size={40} iconStyle={{ marginTop: 5 }} />
          </TouchableOpacity>
        </View>

        <ScrollView style={{ flexDirection: 'column', backgroundColor: '#ede9e6' }}>
          {
            // Upper: ... Lower: ...
            this.state.savedOutfits.map((obj, index) => {
              outfitItemKeys = getItemKeys(obj);
              console.log('item keys: ', outfitItemKeys);

              return (
                <View style={{}}>
                  <Text style={styles.topTitleBold}>Outfit {index}</Text>
                  <Text style={styles.topTitle}>Date: {this.state.savedOutfitDates[index].toString().substring(0,10)}</Text>
                  {
                    getItems(obj).map((item, i) => {
                      console.log('item: ', index, item)
                      return (
                        <View key={i} style={{ flexDirection: 'row', margin: 30, backgroundColor: 'white', height: 160, borderRadius: 15, width: 330 }}>
                          <Card
                            title={item.clothingClass}
                            containerStyle={{ width: 100, height: 150, marginLeft: 15, marginTop: 5, borderRadius: 15, borderColor: '#AEA699' }}
                            titleStyle={{ color: '#AEA699', fontSize: 20 }}
                            image={{ uri: 'data:image/png;base64,' + item.imageData }}
                            imageStyle={{ width: 75, height: 75, alignSelf: 'center', borderRadius: 15 }}
                          >
                            <View style={{ flexDirection: 'row', alignSelf: 'center' }} onPress={() => console.log('the view was pressed')}>
                              {/* <Button
                            icon={<Icon name='cancel' color='white' />}
                            onPress={removeClothingItem(this.state.itemKeys[index])}
                            buttonStyle={{ borderRadius: 15, marginLeft: 0, marginRight: 0, backgroundColor: '#AEA699' }}
                            title='Remove' /> */}
                            </View>

                          </Card>
                          <View style={{ flex: 0, flexDirection: 'column', width: 200, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ flex: 1 }}>
                              <Text style={styles.midTitle}>attributes</Text>
                            </View>
                            <ScrollView style={{ marginTop: 35 }}>
                              <Tags
                                initialText=""
                                textInputProps={{
                                  placeholder: "tags of clothes"
                                }}
                                readonly={true}
                                deleteTagOnPress={false}
                                initialTags={R.values(item.attributes)}
                                onChangeTags={tags => console.log(tags)}
                                containerStyle={{ flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', alignContent: 'flex-start' }}
                                inputStyle={{ backgroundColor: "white" }}
                                renderTag={({ tag, index, onPress, deleteTagOnPress, readonly }) => (
                                  <TouchableOpacity key={`${tag}-${index}`} onPress={onPress} style={{ margin: 5, borderRadius: 50, backgroundColor: '#ebebeb', padding: 10 }}>
                                    <Text>{tag}</Text>
                                  </TouchableOpacity>
                                )}
                              />
                            </ScrollView>
                          </View>
                        </View>


                      )
                    })
                  }
                </View>
              )
            })
          }

          <View style={{ height: 100 }}></View>

        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  midTitle: {
    textAlign: 'center',
    fontFamily: 'sans-serif-thin',
    fontWeight: '200',
    fontSize: 20,
    color: '#AEA699',
    marginTop: 5
  },
  topTitle: {
    textAlign: 'center',
    fontFamily: 'sans-serif-thin',
    fontWeight: '200',
    fontSize: 20,
    color: '#AEA699',
    margin: 10
  },
  topTitleBold: {
    textAlign: 'center',
    fontFamily: 'sans-serif',
    fontWeight: '200',
    fontSize: 35,
    color: '#AEA699',
    margin: 10
  }
})




export default StylesScreen