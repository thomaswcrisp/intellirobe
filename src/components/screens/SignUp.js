import React from 'react';
import { StyleSheet, Text, TextInput, View, Button, TouchableOpacity, Keyboard } from 'react-native';
import HeaderText from '../MyAppHeaderText';
import firebase from 'react-native-firebase'

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = { email: '', password: '', errorMessage: null, displayIntro: true, marginText: '30%' };
    }

    handleSignUp = () => {
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => this.props.navigation.navigate('AppContainer'))
            .catch(error => this.setState({ errorMessage: error.message
        }))
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ displayIntro: false, marginText: '10%' })
    }
    
      _keyboardDidHide () {
          this.setState({ displayIntro: true, marginText: '30%'})
    }

    render() {
        return (
            <View style={styles.container}>
            <Text style={styles.mainTitleStyle}>Intellirobe</Text>
            {this.state.displayIntro && <Text style={{color: '#AEA699', fontFamily: 'sans-serif-thin', fontSize: 18, padding: 15, textAlign: 'center' }}>your own artificially intelligent wardrobe assistant is but a click away...</Text>}
                <View style={{flex: 2, justifyContent: 'center', alignItems: 'center', marginBottom: this.state.marginText }}>
                    <Text style={styles.headerStyle}>Sign Up</Text>
                    {/* if error message is non-null */}
                    {this.state.errorMessage &&
                        <Text style={{ color: 'red' }}>
                            {this.state.errorMessage}
                        </Text>}
                    <TextInput
                        placeholder="Email"
                        autoCapitalize="none"
                        style={styles.textInput}
                        // set the state of the TextInput to equal the text input onChangeText
                        onChangeText={email => this.setState({ email:  email })}
                        onSubmitEditing={Keyboard.dismiss}
                        value={this.state.email}
                    />
                    <TextInput
                        secureTextEntry
                        placeholder="Password"
                        autoCapitalize="none"
                        style={styles.textInput}
                        onSubmitEditing={Keyboard.dismiss}
                        onChangeText={password => this.setState({ password })}
                        value={this.state.password}
                    />
                    <TouchableOpacity onPress={this.handleSignUp} style={styles.buttonStyle}>
                        <Text style={{color: 'white', fontFamily: 'sans-serif-thin', fontSize: 18, padding: 5}}>Sign Up</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        style={styles.buttonStyle}
                        onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={{color: 'white', fontFamily: 'sans-serif-thin', fontSize: 18, padding: 5}}>Already have an account? Login</Text>
                        </TouchableOpacity>
                </View>
             </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderRadius: 15,
        textAlign: 'center',
        borderWidth: 1,
        marginTop: 8,
        // backgroundColor: '#EFF0F1'
    },
    headerStyle: {
        textAlign: 'center', 
        fontFamily: 'sans-serif-thin', 
        fontWeight:'200', 
        fontSize: 45, 
        color: '#AEA699', 
        marginTop: 50,
        // borderRadius: 40, 
        // borderBottomWidth: 2, 
        // borderBottomColor: '#AEA699'
    },
    mainTitleStyle: {
        textAlign: 'center', 
        fontFamily: 'serif', 
        fontWeight:'200', 
        fontSize: 65, 
        width: '100%',
        // backgroundColor: '#EFF0F1',
        alignItems: 'center',
        // marginBottom: '60%',
        marginTop: 100,
        justifyContent: 'center',
        color: '#AEA699', 
        marginTop: 5,
        borderRadius: 100, 
        borderBottomWidth: 26, 
        borderBottomColor: '#AEA699'
    },
    buttonStyle: {
        borderRadius: 15, 
        marginTop: 15,
        margin: 5, 
        padding: 3,
        backgroundColor: '#AEA699'
    }
})