import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { createStackNavigator } from "react-navigation";
import ClassifierScreen from './Classifier';
import HeaderText from '../MyAppHeaderText';
import { Tile, Card, Button, Icon } from 'react-native-elements';
import { ScrollView } from "react-native-gesture-handler";
// import { Icon } from 'react-native-vector-icons/MaterialCommunityIcons';
import WardrobeScreen from './WardrobeScreen';
import firebase from 'react-native-firebase';
import NewWardrobe from './NewWardrobe';
import R from 'ramda';

const getWardrobeNames = R.compose(
    R.values
);

const getWardrobeKeys = R.compose(
    R.keys
)

class WardrobeHome extends React.Component {

    // Render tiles for each wardrobe saved 

    static navigationOptions = {
        headerTitle: <HeaderText>Wardrobe Portal</HeaderText>,
        headerTintColor: '#AEA699',
    }

    constructor(props) {
        super(props);

        this.state = {
            wardrobes: [],
            currentUser: null,
            wardrobeKeys: [],
        }

        // firebase.database().ref('wardrobes/' + firebase.auth().currentUser.uid + '/').on('value', (data) => {
        //     data = data.toJSON();
        //     console.log(getWardrobeNames(data));
        //     console.log(getWardrobeKeys(data));
        //     this.setState({ wardrobes: getWardrobeNames(data), wardrobeKeys: getWardrobeKeys(data) });
        // })

    }


    componentDidMount() {
        const { currentUser } = firebase.auth();
        this.setState({ currentUser });
        console.log('inside the component did mount for wardrobeHome');
        console.log(currentUser);
        console.log('uid ', firebase.auth().currentUser.uid);

        // firebase.database().goOnline();

        firebase.database().ref('wardrobes/' + firebase.auth().currentUser.uid + '/').on('value', (data) => {
            console.log('ref[value]');
            data = data.toJSON();
            console.log(getWardrobeNames(data));
            console.log(getWardrobeKeys(data));
            this.setState({ wardrobes: getWardrobeNames(data), wardrobeKeys: getWardrobeKeys(data) });
        }, (error) => {
            console.log(error);
        })


        // firebase.database().ref('wardrobes/' + firebase.auth().currentUser.uid + '/').on('value', (data) => {
        //     data = data.toJSON();
        //     console.log(getWardrobeNames(data));
        //     console.log(getWardrobeKeys(data));
        //     this.setState({ wardrobes: getWardrobeNames(data), wardrobeKeys: getWardrobeKeys(data) });
        // }, (error) => {console.log(error)})
    }

    render() {
        console.log(this.props.navigation)
        console.log(this.state.currentUser)
        console.log(this.state.wardrobes);
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start', backgroundColor: '#ede9e6' }}>
                <View style={{ flex: 1, textAlign: 'center', flexDirection: "column", marginTop: 50, backgroundColor: '#ede9e6' }}>

                    <Tile
                        height={150}
                        style={styles.newWardrobeStyle}
                        iconContainerStyle={{ alignItems: "flex-start" }}
                        captionStyle={{ fontFamily: 'sans-serif-thin', fontSize: 20, fontWeight: 'bold' }}
                        imageSrc={require('../../../img/wardrobe.jpg')}
                        title="Add New Wardrobe"
                        featured
                        onPress={() => this.props.navigation.navigate('NewWardrobe')}
                        caption="click here to add a new wardrobe"
                    />
                    <Text style={styles.titleTextBody}>Saved Wardrobes</Text>
                </View>

                <View style={{ flex: 2, alignItems: "center", justifyContent: "flex-start", backgroundColor: '#ede9e6', flexDirection: "column" }}>
                    <ScrollView style={{ flex: 1, textAlign: 'center' }}>
                        {
                            this.state.wardrobes.map((obj, index) => {
                                // firebase
                                //     .database()
                                //     .ref('wardrobes/' + firebase.auth().currentUser.uid + '/')
                                //     .child(this.state.wardrobeKeys[index])
                                //     .remove()
                                //     .then(x => { })
                                //     .catch(e => { })


                                // const removeWardrobe = () => firebase
                                //     .database()
                                //     .ref('wardrobes/' + firebase.auth().currentUser.uid + '/')
                                //     .child(this.state.wardrobeKeys[index])
                                //     .remove()
                                //     .then(result => {
                                //         console.log(result);
                                //     })
                                //     .catch(err => {
                                //         console.log(err);
                                //     })

                                const removeWardrobe = index => () => {
                                    firebase
                                        .database()
                                        .ref('wardrobes/' + firebase.auth().currentUser.uid + '/')
                                        .child(this.state.wardrobeKeys[index])
                                        .remove()
                                        .then(result => {
                                            console.log(result);
                                        })
                                        .catch(err => {
                                            console.log(err);
                                        })
                                };
                                if (this.state.wardrobeKeys[index] != 'savedOutfits') {
                                    return (
                                        <Card
                                            title={obj.wardrobeName} titleStyle={{ color: '#AEA699' }} key={index}>
                                            {/* image={require('../../img/wardrobeIcon.png')}> */}
                                            <Text style={{ marginBottom: 10, textAlign: "center" }}>
                                                {obj.description}
                                            </Text>
                                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                <Button
                                                    icon={<Icon name='accessibility' color='white' />}
                                                    backgroundColor='#AEA699'
                                                    onPress={() => this.props.navigation.navigate('WardrobeScreen', { wardrobeID: this.state.wardrobeKeys[index], wardrobeName: obj.wardrobeName })}
                                                    buttonStyle={{ borderRadius: 15, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor: '#AEA699', marginRight: 10 }}
                                                    title='Visit Wardrobe'
                                                />
                                                <Button
                                                    //    the elements library uses MaterialIcons library 
                                                    icon={<Icon name='cancel' color='white' />}
                                                    backgroundColor='#AEA699'
                                                    onPress={removeWardrobe(index)}
                                                    buttonStyle={{ borderRadius: 15, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor: 'red' }}
                                                    title='Delete' />
                                            </View>
                                        </Card>)
                                }
                            })
                        }

                        {/* <TouchableOpacity 
                    style={{margin:10, backgroundColor:'#d3d9e3', padding:10, borderRadius:10}}
                    onPress={() => this.props.navigation.navigate('Classifier')}><Text>My Wardrobes</Text></TouchableOpacity>
                    <TouchableOpacity 
                    style={{margin:10, backgroundColor:'#d3d9e3', padding:10, borderRadius:10}}
                    onPress={() => this.props.navigation.navigate('Classifier')}><Text>Recommender</Text></TouchableOpacity> */}
                    </ScrollView>

                </View>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer: {
        flex: 1,
    },
    newWardrobeStyle: {
        height: 50,
        flex: 1,
        flexDirection: 'row',
        alignItems: "flex-start",
        justifyContent: 'flex-start'
    },
    titleText: {
        textAlign: 'center',
        flex: 1,
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 35,
        color: '#AEA699',
        marginTop: 5,
        borderRadius: 40,
        borderBottomWidth: 2,
        borderBottomColor: '#AEA699'

    },
    titleTextBody: {
        textAlign: 'center',
        flex: 1,
        fontFamily: 'sans-serif-thin',
        fontWeight: '200',
        fontSize: 35,
        color: '#AEA699',
        // marginTop: 5, 
        margin: 5,
        // borderRadius: 40, 
        // borderBottomWidth: 2, 
        // borderBottomColor: '#AEA699'

    }
})

const WardrobeStack = createStackNavigator({
    Wardrobe: {
        screen: WardrobeHome
    },
    WardrobeScreen: {
        screen: WardrobeScreen
    },
    Classifier: {
        screen: ClassifierScreen
    },
    NewWardrobe: {
        screen: NewWardrobe
    }

})



export default WardrobeStack