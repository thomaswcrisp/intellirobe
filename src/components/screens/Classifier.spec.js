import React from "react";
import { shallow } from "enzyme";
import Enzyme from "enzyme";
import sinon from "sinon";

jest.mock("../../modules/classifier");
const classifierModule = require("../../modules/classifier");

import Classifier from "./Classifier";

describe("Classifier", () => {
  test("useClassifier hook is called with the wardrobe id from the navigation param", () => {
    const fakeClassifierState = {
      state: {}
    };
    const fakeProps = {
      navigation: {
        getParam: () => "fake_wardrobe_param"
      }
    };
    const fakeClassifier = sinon.stub().returns(fakeClassifierState);
    classifierModule.useClassifier.mockImplementation(fakeClassifier);
    shallow(<Classifier {...fakeProps} />);

    expect(fakeClassifier.calledWith("fake_wardrobe_param")).toBeTruthy();
  });

  test("Dropdown and Tags component should only be rendered when the isClassified flag is true", () => {
    const fakeClassifierState = {
      state: {
        isClassified: true,
        predictedCategories: [],
        attributes: []
      },
    };
    const fakeProps = {
      navigation: {
        getParam: () => null
      }
    };
    const fakeClassifier = sinon.stub().returns(fakeClassifierState);
    classifierModule.useClassifier.mockImplementation(fakeClassifier);
    const target = shallow(<Classifier {...fakeProps} />);

    expect(target.find("Dropdown").length).toBe(1);
    expect(target.find("Tags").length).toBe(1);
  });

  test("expected props are passed down to the Dropdown component", () => {
    const fakeSelectCategory = sinon.spy();
    const fakePredictedCategory = 'fake category';
    const fakeClassifierState = {
      state: {
        isClassified: true,
        predictedCategories: [fakePredictedCategory],
        attributes: []
      },
      selectCategory: fakeSelectCategory,
    };
    const fakeProps = {
      navigation: {
        getParam: () => null
      }
    };
    const fakeClassifier = sinon.stub().returns(fakeClassifierState);
    classifierModule.useClassifier.mockImplementation(fakeClassifier);
    const target = shallow(<Classifier {...fakeProps} />).find('Dropdown');
    
    expect(target.prop('data')).toEqual([
      { value: fakePredictedCategory },
    ]);

    target.prop('onChangeText')('fake change');

    expect(fakeSelectCategory.firstCall.args).toEqual(['fake change']);
  });
});
