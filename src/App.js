/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, PureComponent} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { RNCamera } from 'react-native-camera'
import Camera from './components/camera/Camera'
import Classifier from './components/screens/Classifier'
import { createAppContainer } from 'react-navigation';

import AppNavigator from './components/ScreenManager'
import LoginNavigator from './components/LoginScreenManager'


const LoginContainer = createAppContainer(LoginNavigator)
const AppContainer = createAppContainer(AppNavigator)

class App extends PureComponent {


  render() {
    return (
      // <View style={{width:'100%', height:'100%'}}>
      //   <View style={styles.container}>
      //   <Camera />
      //   </View>
      //   {/*FOR BUTTON https://github.com/jacklam718/react-native-button-component
      //   https://github.com/react-native-community/react-native-linear-gradient */}
      //   <View style={{flex:2, flexDirection: 'column'}}>
      //     <Text>This is just some text</Text>
      //   </View>
      // </View>
      // <Classifier username='Tom' attributes={['striped','floral','dotted']}/>
      <LoginContainer />
    );
  }


}

const styles = StyleSheet.create({
  container: {
    height:'50%',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  preview: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
    bottom: 45,
  },
  capture: {
    flex: 0,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent:'flex-start',
    margin: 10,
  },
});

export default App