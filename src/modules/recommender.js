import R from 'ramda';
import { useReducer, useCallback, useEffect } from 'react';

import firebase from "react-native-firebase";

// start extraction

import moment from 'moment';
import md5 from 'md5';

import weatherCategories from "../content/recommender/weather-categories.json";
import clothingCategories from "../content/recommender/clothing-categories.json";
import { pickRandomElement } from "../utils";

const getItemKeys = R.compose(R.keys);

const getItems = R.values;

export const getWardrobeByName = R.useWith(R.find, [
  R.propEq("wardrobeName"),
  R.identity
]);

export const pickClothingItemClass = R.compose(
  pickRandomElement,
  R.useWith(R.filter, [
    R.propEq("clothingClass"),
    R.compose(
      getItems,
      R.prop("clothes")
    )
  ])
);

const pickClothingItem = (type, wardrobeSelected, compatibleCategories) =>
  R.compose(
    pickRandomElement,
    R.filter(
      obj =>
        obj.clothingType == type.toLowerCase() &&
        obj.clothingClass != "Jacket" &&
        obj.clothingClass != "Coat" &&
        R.contains(obj.clothingClass, compatibleCategories)
    ),
    getItems
  )(wardrobeSelected.clothes);

const checkClothingTypeCount = (type, wardrobeSelected, compatibleCategories) => {
  let counter = 0;
  getItems(wardrobeSelected.clothes).map((obj, index) => {
    if (
      obj.clothingType == type.toLowerCase() &&
      R.contains(obj.clothingClass, compatibleCategories)
    ) {
      counter = counter + 1;
    }
  });
  return counter;
};

const checkClothingTypeExists = R.curry((type, wardrobeSelected, compatibleCategories) => R.compose(
  R.any(obj => (
    obj.clothingType == type.toLowerCase() &&
    R.contains(obj.clothingClass, compatibleCategories)
  )),
  getItems
)(wardrobeSelected.clothes));


const getAttributeHash = R.compose(
  md5,
  R.apply(R.concat),
  R.sortBy(R.identity)
);

export const getCommonAttributePairs = R.compose(
  R.filter(R.is(Array)),
  R.values,
  R.apply(R.mergeWith(R.pair)),
  R.unapply(R.pluck("attributes"))
);

const checkClothingExists = R.useWith(R.any, [
  R.equals,
  R.compose(
    R.pluck("clothingClass"),
    getItems,
    R.prop("clothes")
  )
]);

const checkOutfitDateCompatibility = R.complement(
  R.useWith(R.any, [
    outfit => {
      const now = moment();
      const outfitWithin3Days = ({ date }) => now.diff(moment(date), "days") <= 3;
      return R.compose(R.both(R.propEq("outfit", outfit), outfitWithin3Days));
    },
    R.values
  ])
);

const fetchRecommendation = async attributePairs => {
  const response = await fetch("http://192.168.0.32/recommendation", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      attributePairs
    })
  });
  return await response.json();
};

const temperatureMapping = {
  15: "cold",
  25: "med",
  Infinity: "hot"
};

const getWeatherCategory = R.compose(
  R.cond,
  R.map(([temperature, category]) => [
    R.lt(R.__, temperature),
    R.always(category)
  ]),
  R.toPairs
)(temperatureMapping);

const dressTypeWeatherMapping = R.compose(
  R.map(
    R.compose(
      R.map(R.__, weatherCategories),
      R.intersection
    )
  )
)(clothingCategories);

const getCompatibleClothingCategories = R.compose(
  R.path(R.__, dressTypeWeatherMapping),
  R.over(R.lensIndex(0), R.toLower),
  R.pair
);

const getStaticRecommendation = (weatherCategory, selectedWardrobe) => {
  const coat = pickClothingItemClass("Coat", selectedWardrobe);
  const jacket = pickClothingItemClass("Jacket", selectedWardrobe);
  const staticRecommendation = {};
  if (weatherCategory === "cold" && coat) {
    staticRecommendation.Coat = coat;
  } else if (weatherCategory === "med" && jacket) {
    staticRecommendation.Jacket = jacket;
  }
  return staticRecommendation;
};


// end extraction


export const RESET = 'RESET';
export const SET_RECOMMENDING = 'SET_RECOMMENDING';
export const PICK_OUTFIT = 'PICK_OUTFIT';
export const UPDATE_RECOMMENDATION = 'UPDATE_RECOMMENDATION';

const reducerActions = {
  [RESET]: state => R.merge(state, {
    temperature: 0,
    initiateRecommend: false,
    outfit: {},
    wardrobeSelected: {},
  }),
  [SET_RECOMMENDING]: state => ({
    ...state,
    initiateRecommend: true,
    processing: true
  }),
  [PICK_OUTFIT]: (state, { outfit, processing }) => ({
    ...state,
    outfit,
    processing,
  }),
  [UPDATE_RECOMMENDATION]: (state, { payload }) => ({
    ...state,
    ...payload,
  })
};

export const reducer = (state, action) => {
    if (reducerActions[action.type]) {
        return reducerActions[action.type](state, action);
    }
    return state;
};

export const initialState = () => ({
  temperature: 0,
  initiateRecommend: false,
  outfit: {},
  selectedWardrobe: {},
  compatibleCategories: [],
  dressType: "",
  wardrobes: [],
  processing: false,
  savedOutfits: {},
});

export const useRecommender = (props) => {
    const [state, dispatch] = useReducer(reducer, null, initialState);

    const {
      outfit,
      wardrobes,
      dressType,
      weatherCategory,
      selectedWardrobe,
      savedOutfits,
      compatibleCategories,
      staticRecommendation,
    } = state;

    useEffect(() => {
      firebase
        .database()
        .ref("wardrobes/" + firebase.auth().currentUser.uid + "/")
        .on("value", data => {
          data = data.toJSON();
          const wardrobes = R.compose(
            R.values,
            R.dissoc("savedOutfits")
          )(data);
          dispatch({
            type: UPDATE_RECOMMENDATION,
            payload: {
              wardrobes,
              selectedWardrobe: R.propOr({}, 0, wardrobes),
              savedOutfits: R.prop("savedOutfits", data)
            }
          });
        });
    }, []);


    const pickComplimentingItem = async (selectedWardrobe, outfit) => {
      const selectedWardobeCompatibleCategories = compatibleCategories;
      const compatibleClothingItems = R.compose(
        R.map(clothingItem => ({
          ...clothingItem,
          attributePairs: getCommonAttributePairs(outfit.Upper, clothingItem).map(
            getAttributeHash
          )
        })),
        R.filter(
          R.allPass([
            R.propEq("clothingType", "lower"),
            R.propSatisfies(
              R.contains(R.__, selectedWardobeCompatibleCategories),
              "clothingClass"
            ),
            R.compose(
              checkOutfitDateCompatibility(R.__, savedOutfits),
              R.merge(outfit),
              R.objOf("Lower")
            )
          ])
        ),
        getItems
      )(selectedWardrobe.clothes);
  
      const data = await fetchRecommendation(
        R.pluck('attributePairs', compatibleClothingItems)
      );
  
      return R.compose(
        R.when(R.isEmpty, R.always(undefined)),
        R.dissoc('weight'),
        R.dissoc('attributePairs'),
        R.reduce(R.maxBy(R.prop("weight")), { weight: -Infinity }),
        R.zipWith((clothingItem, weight) => ({
          ...clothingItem,
          weight
        }))
      )(compatibleClothingItems, data.weights);
    };

    let pickOutfit;
    pickOutfit = (prevPicked = []) => {
      dispatch({ type: SET_RECOMMENDING });
      const wardrobeHasClothingType = checkClothingTypeExists(R.__, selectedWardrobe, compatibleCategories);
      if (
        wardrobeHasClothingType('Overall') &&
        !wardrobeHasClothingType('Upper')
      ) {
        dispatch({
          type: PICK_OUTFIT,
          outfit: {
            ...outfit,
            Overall: pickClothingItem(
              "Overall",
              selectedWardrobe,
              compatibleCategories
            )
          },
          processing: false
        });
      } else if (
        wardrobeHasClothingType('Overall') &&
        Math.random() < 0.5
      ) {
        // store all previous state in udpatedOutfit
        let updatedOutfit = { ...outfit };
        updatedOutfit.Overall = pickClothingItem(
          "Overall",
          selectedWardrobe,
          compatibleCategories
        );
        dispatch({
          type: PICK_OUTFIT,          
          outfit: updatedOutfit,
          processing: false
        });
      } else if (wardrobeHasClothingType('Upper')) {
        let updatedOutfit = { ...outfit };
        updatedOutfit.Upper = pickClothingItem(
          "Upper",
          selectedWardrobe,
          compatibleCategories
        );
        pickComplimentingItem(selectedWardrobe, updatedOutfit)
          .then(result => {
            updatedOutfit = {
              ...staticRecommendation,
              ...updatedOutfit,
              Lower: result,
            };
            
            if (updatedOutfit.Lower != undefined) {
              dispatch({
                type: PICK_OUTFIT,
                outfit: updatedOutfit,
                processing: false
              });
            } else {
              if (
                prevPicked.length + 1 >=
                checkClothingTypeCount("Upper", selectedWardrobe, compatibleCategories)
              ) {
                dispatch({
                  type: PICK_OUTFIT,
                  outfit: {},
                  processing: false
                });
              } else if (R.contains(updatedOutfit.Upper, prevPicked)) {
                pickOutfit(prevPicked);
              } else {
                prevPicked.push(updatedOutfit.Upper);
                pickOutfit(prevPicked);
              }
            }
          })
          .catch(e => {
            console.log("error ", e);
          });
      } else {
        dispatch({
          type: PICK_OUTFIT,
          outfit: {},
          processing: false
        });
      }
    };

    const setRecommendationState = (dressType, temperature) => {
      const weatherCategory = getWeatherCategory(temperature);
      const compatibleCategories = getCompatibleClothingCategories(
        dressType,
        weatherCategory
      );
      dispatch({
        type: UPDATE_RECOMMENDATION,
        payload: {
          dressType: R.toLower(dressType),
          weatherCategory,
          compatibleCategories,
          temperature,
          staticRecommendation: getStaticRecommendation(
            weatherCategory,
            selectedWardrobe
          )
        }
      });
    };
  
    const selectWardrobe = wardrobeName => {
      const selectedWardrobe = getWardrobeByName(
        wardrobeName,
        wardrobes
      );
  
      dispatch({
        type: UPDATE_RECOMMENDATION,
        payload: {
          selectedWardrobe,
          staticRecommendation: getStaticRecommendation(
            weatherCategory,
            selectedWardrobe
          )
        }
      });
    };
    
    const resetPage = useCallback(() => dispatch({ type: RESET }), []);

    const saveOutfit = () => {
      if (checkOutfitDateCompatibility(outfit, savedOutfits)) {
        firebase
          .database()
          .ref(`wardrobes/${firebase.auth().currentUser.uid}/savedOutfits/`)
          .push({
            outfit,
            date: moment()
          });
      } else {
        alert("cannot save a recently recommended outfit");
      }
      resetPage();
      props.navigation.navigate("Wardrobe");
    };

    const renderRecommender = () => {
      return dressType != "" && wardrobes.length > 0
        ? pickOutfit()
        : alert(
            "please first select a dress type and make sure you have created a wardrobe"
          );
    };
  
    const resetAndRenderRecommender = () => {
      resetPage();
      return dressType != "" && wardrobes.length > 0
        ? pickOutfit()
        : alert(
            "please first select a dress type and make sure you have created a wardrobe"
          );
    };

    return {
        state,
        resetPage,
        pickComplimentingItem,
        setRecommendationState,
        selectWardrobe,
        saveOutfit,
        renderRecommender,
        resetAndRenderRecommender,
    };
};
