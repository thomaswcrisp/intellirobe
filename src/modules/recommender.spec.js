import { testHook } from "../test.utils";
import {
  getCommonAttributePairs,
  getWardrobeByName,
  pickClothingItemClass
} from "./recommender";

describe("modules/recommender", () => {
  test("getCommonAttributePairs gets common attribute pairs from 2 objects", () => {
    const result = getCommonAttributePairs(
      {
        attributes: {
          MatchingAttribute: "attribute",
          NonMatchingAttribute: "attribute"
        }
      },
      {
        attributes: {
          MatchingAttribute: "attribute",
          AnotherNonMatchingAttribute: "attribute"
        }
      }
    );
    expect(result).toMatchSnapshot();
  });
  test("getWardrobeByName gets a wardobe with a provided name from a list of wardrobes", () => {
    const result = getWardrobeByName("me", [
      { wardrobeName: "not me" },
      { wardrobeName: "me", other: "information" },
      { wardrobeName: "not me" }
    ]);
    expect(result).toMatchSnapshot();
  });
  test("pickClothingItemClass gets a random clothing item that matches a given clothingClass", () => {
    const result = pickClothingItemClass("me", {
      clothes: {
        one: { clothingClass: "not me" },
        two: { clothingClass: "me", other: "information" },
        three: { clothingClass: "not me" }
      }
    });
    expect(result).toMatchSnapshot();
  });
});
