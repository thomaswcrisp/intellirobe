import R from 'ramda';
import { useReducer, useCallback } from 'react';

import { getUserId } from '../providers/auth';
import { addWardrobeItem } from '../providers/store';

export const PROCESS_PREDICTION = 'PROCESS_PREDICTION';
export const REMOVE_ATTRIBUTE = 'REMOVE_ATTRIBUTE';
export const SELECT_CATEGORY = 'SELECT_CATEGORY';

const reducerActions = {
    [PROCESS_PREDICTION]: (state, { payload: { imageIcon, predictions: { category, ...remainingAttributes }, base64 }}) => ({
        ...state,
        predictedCategories: [...category],
        selectedCategory: category[0],
        attributes: R.compose(
            R.map(R.zipObj(['name', 'prediction'])),
            R.toPairs
        )(remainingAttributes),
        imageIcon: imageIcon,
        isClassified: true,
        imageData: base64,
    }),
    [REMOVE_ATTRIBUTE]: (state, { index }) => R.dissocPath(['attributes', index], state),
    [SELECT_CATEGORY]: (state, { category }) => R.assoc('selectedCategory', category, state),
};
export const reducer = (state, action) => {
    if (reducerActions[action.type]) {
        return reducerActions[action.type](state, action);
    }
    return state;
};

export const initialState = wardrobeID => ({
    wardrobeID,
    attributes: [],
    attributesObj: {},
    selectedCategory: '',
    imageIcon: '',
    isClassified: false,
    imageData: '',
    userId: getUserId(),
});

export const useClassifier = wardrobeID => {
    const [state, dispatch] = useReducer(reducer, wardrobeID, initialState);

    const addClassifiedItem = () => addWardrobeItem({
        userId: state.userId,
        wardrobeID: state.wardrobeID,
        imageData: state.imageData,
        attributes: R.compose(
            R.fromPairs,
            R.map(R.props(['name', 'prediction']))
        )(state.attributes),
        selectedCategory: state.selectedCategory,
    });

    return {
        state,
        processPrediction: useCallback((imageIcon, predictions, base64) => dispatch({ type: PROCESS_PREDICTION, payload: { imageIcon, predictions, base64 } }), []),
        removeAttribute: useCallback(index => dispatch({ type: REMOVE_ATTRIBUTE, index }), []),
        selectCategory: useCallback(category => dispatch({ type: SELECT_CATEGORY, category }), []),
        addClassifiedItem,
    };
};
