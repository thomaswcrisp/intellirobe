import { testHook } from "../test.utils";

jest.mock("../providers/auth");
const authProvider = require("../providers/auth");

import { initialState, useClassifier } from "./classifier";

describe("modules/classifier", () => {
  test("initialState function accepts wardrobe id as first argument and returns correct object containing both wardrobe id and user id", () => {
    authProvider.getUserId.mockImplementation(() => "fake_user_id");
    expect(initialState("fake_wardrobe_id")).toMatchSnapshot();
  });

  let useClassifierHook;

  beforeEach(() => {
    testHook(() => {
      useClassifierHook = useClassifier("fake_wardrobe_id");
    });
  });
  test("initial classifier state is initialised with current user id, wardrobe id and expected initial state", () => {
    const { state } = useClassifierHook;

    expect(state).toMatchSnapshot();
  });
  test("processPrediction populates the state with the provided prediction with the desired structure", () => {
    const { processPrediction } = useClassifierHook;

    const fakePredictions = {
      category: ["fake category 1", "fake category 2", "fake category 3"],
      fake_attribute_1: "fake prediction 1",
      fake_attribute_2: "fake prediction 2",
      fake_attribute_3: "fake prediction 3"
    };
    processPrediction("fake_image_icon", fakePredictions, "fake_image_base_64");

    const { state } = useClassifierHook;
    expect(state).toMatchSnapshot();
  });
  test("removeAttribute removes an attribute at a given index from the attributes array", () => {
    const { processPrediction } = useClassifierHook;

    const fakePredictions = {
      category: ["fake category 1", "fake category 2", "fake category 3"],
      fake_attribute_1: "fake prediction 1",
      fake_attribute_2: "fake prediction 2",
      fake_attribute_3: "fake prediction 3"
    };
    processPrediction("fake_image_icon", fakePredictions, "fake_image_base_64");

    const { removeAttribute } = useClassifierHook;
    removeAttribute(1);
    const { state } = useClassifierHook;
    expect(state).toMatchSnapshot();
  });
  test("selectCategory sets the selectedCategory in the state to the provided value", () => {
    const { selectCategory } = useClassifierHook;
    selectCategory("fake_category");
    const { state } = useClassifierHook;
    expect(state).toMatchSnapshot();
  });
});
