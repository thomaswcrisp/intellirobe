import R from 'ramda';

import { pickRandomElement } from "./utils";

describe("utils", () => {
  it("pickRandomElement should pick a random element from a given array", () => {
    const fakeArray = ['fake element 1', 'fake element 2', 'fake element 3'];

    expect(fakeArray).toContain(pickRandomElement(fakeArray));
    expect(fakeArray).toContain(pickRandomElement(fakeArray));
    expect(fakeArray).toContain(pickRandomElement(fakeArray));
    expect(fakeArray).toContain(pickRandomElement(fakeArray));
    expect(fakeArray).toContain(pickRandomElement(fakeArray));
  });
});
