import R from 'ramda';

import typeCategoryMapping from '../content/classifier/type-category-mapping.json';

export const getCategoryFromType = R.compose(
    R.flip(R.prop),
    R.mergeAll,
    R.map(([category, types]) => types.reduce((acc, val) => ({ ...acc, [val]: category }), {})),
    R.toPairs,
)(typeCategoryMapping);
