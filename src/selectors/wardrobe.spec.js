import { getCategoryFromType } from './wardrobe';

describe('selectors/wardrobe', () => {
    it('returns lower for a category that belongs to the lower type', () => {
        expect(getCategoryFromType('Skirt')).toEqual('lower');
    });
    it('returns overall for a category that belongs to the overall type', () => {
        expect(getCategoryFromType('Dress')).toEqual('overall');
    });
    it('returns upper for a category that belongs to the upper type', () => {
        expect(getCategoryFromType('Shirt')).toEqual('upper');
    });
});
