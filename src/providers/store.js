import firebase from 'react-native-firebase';
import R from 'ramda';

import { getCategoryFromType } from '../selectors/wardrobe';

export const addWardrobeItem = ({ userId, wardrobeID, imageData, attributes, selectedCategory }) => firebase
    .database()
    .ref(`wardrobes/${userId}/${wardrobeID}/clothes/`)
    .push({
        imageData,
        attributes,
        clothingType: getCategoryFromType(selectedCategory),
        clothingClass: selectedCategory
    });

