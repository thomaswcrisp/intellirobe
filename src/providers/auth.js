import firebase from 'react-native-firebase';
import { getCategoryFromType } from '../selectors/wardrobe';

export const getUserId = () => firebase.auth().currentUser.uid;
