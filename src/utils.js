import R from 'ramda';

export const pickRandomElement = R.converge(R.nth, [
  R.compose(Math.round, length => Math.random() * length, R.subtract(R.__, 1), R.length),
  R.identity
]);
