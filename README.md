# Intellirobe

TODO

### Troubleshooting

#### Gradle Permissions Issue

When debugging the project you may encounter an error stating `Error while executing command 'react-native run-android --no-packager'`. This could be permission related, if you run `react-native run-android --no-packager --verbose`, you can confirm whether this is the case. To resolve grant executable permissions to the `gradlew` file so the shell script can be executed:
```
chmod +x android/gradlew
```

#### Generate Keystore

Certain platforms will require you to generate a keystore in order to sign your android application for debugging. [See this stackoverflow answer describes how to rectify this issue](https://stackoverflow.com/a/57438549).

